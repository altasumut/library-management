package com.library.management;

import com.library.management.entity.Borrow;
import com.library.management.entity.Member;
import com.library.management.model.dto.BorrowDto;
import com.library.management.model.dto.MemberDto;
import com.library.management.repository.MemberRepository;
import com.library.management.service.BorrowService;
import com.library.management.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@Slf4j
class MemberServiceTest {

    /**
     * All data will be loaded before test
     */

    @Autowired
    private MemberService memberService;

    @Autowired
    private BorrowService borrowService;

    @Autowired
    private MemberRepository memberRepository;

    @Test
    void createNewMember() {
        Member savedMember = memberService.saveMember(new MemberDto("Lorem", "Impus", "m"));
        assertNotNull(savedMember);
    }

    @Test
    void getAllMembers() {
        List<Member> memberList = memberService.getAll();
        log.info("Member list size: {}", memberList.size());
        assertTrue(!memberList.isEmpty());
    }

    @Test
    void getBorrowedMembers() {

        // Retrieved all borrowed members.
        List<Member> memberList = memberService.getBorrowedMembers();
        log.info("Borrowed member list size: {}", memberList.size());
        assertEquals(11, memberList.size());

        // A new member saved.
        Member savedMember = memberService.saveMember(new MemberDto("Lorem", "Impus", "f"));
        assertNotNull(savedMember);

        // Retrieved all borrowed members again.
        assertEquals(11, memberService.getBorrowedMembers().size());

        // Borrowed a book using saved member's id.
        Borrow borrow = borrowService.addBorrow(new BorrowDto(savedMember.getMemberId(), 1, new Date()));

        // Finally we retrieved borrowed members to assert list size increased.
        assertEquals(12, memberService.getBorrowedMembers().size());
    }

    @Test
    void findNonTerminatedNotBorrowedMembers() {

        // Retrieved all borrowed members. - Expected 0
        List<Member> memberList = memberService.getNonTerminatedNotBorrowedMembers();
        log.info("Borrowed member list size: {}", memberList.size());
        assertEquals(0, memberList.size());

        // A new member saved.
        Member savedMember = memberService.saveMember(new MemberDto("Lorem", "Impus", "f"));
        assertNotNull(savedMember);

        // Expected 1 because new member fit for query
        assertEquals(1, memberService.getNonTerminatedNotBorrowedMembers().size());

        // Terminate member's activity
        savedMember.setTerminationDate(new Date());
        memberRepository.save(savedMember);

        // Member is inactive now, List size will return 0
        assertEquals(0, memberRepository.findNonTerminatedNotBorrowedMembers().size());
    }


    @Test
    void getBorrowedMembersByBorrowedDate() throws ParseException {

        // Dummy Member: Elijah Elijah, BorrowDate: 05/01/2008
        // Retrieved all borrowed members by borrow date.
        Date tempDate = new SimpleDateFormat("dd/MM/yyyy").parse("05/01/2008");
        List<Member> memberList = memberService.getBorrowedMembersByBorrowedDate(tempDate);
        log.info("Borrowed member list size: {}", memberList.size());
        assertEquals(1, memberList.size());
        assertEquals("Elijah", memberList.get(0).getFirstName());

        // A new member saved.
        Member savedMember = memberService.saveMember(new MemberDto("Dolar", "Sit Amed", "m"));
        assertNotNull(savedMember);

        // Borrowed a book using saved member's id.
        Borrow borrow = borrowService.addBorrow(new BorrowDto(savedMember.getMemberId(), 1, new Date()));

        // Assert: saved used equals to result
        List<Member> memberListAfterBorrow = memberService.getBorrowedMembersByBorrowedDate(new Date()); // -> today
        log.info("Borrowed member list size: {}", memberList.size());
        log.info("Borrowed member list size: {}", savedMember);
        log.info("Borrowed member list size: {}", memberListAfterBorrow.get(0).toString());
        assertEquals(savedMember, memberListAfterBorrow.get(memberList.size() - 1));
    }
}
