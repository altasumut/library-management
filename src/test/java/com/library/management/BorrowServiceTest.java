package com.library.management;

import com.library.management.entity.Borrow;
import com.library.management.model.dto.BorrowDto;
import com.library.management.service.BorrowService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@Slf4j
class BorrowServiceTest {

    /**
     * All data will be loaded before test
     */

    @Autowired
    private BorrowService borrowService;


    @Test
    void getBorrowById() {
        assertTrue(borrowService.getBorrowById(1).isPresent());
    }

    @Test
    void getAllBorrow() {
        assertTrue(borrowService.getAllBorrows().size() > 0);
    }

    @Test
    void addBorrow() {
        Borrow borrow = borrowService.addBorrow(new BorrowDto(1, 1, new Date()));
        assertNotNull(borrowService.getBorrowById(borrow.getBorrowId()));
    }


    @Test
    void returnBarrow() {
        Borrow borrow = borrowService.addBorrow(new BorrowDto(1, 1, new Date()));
        borrowService.returnBorrow(borrow.getBorrowId());
        assertNotNull(borrowService.getBorrowById(borrow.getBorrowId()));
    }
}