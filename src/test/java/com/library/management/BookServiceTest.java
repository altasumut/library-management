package com.library.management;

import com.library.management.entity.Book;
import com.library.management.entity.Borrow;
import com.library.management.entity.Member;
import com.library.management.model.dto.BookDto;
import com.library.management.model.dto.BorrowDto;
import com.library.management.model.dto.MemberDto;
import com.library.management.service.BookService;
import com.library.management.service.BorrowService;
import com.library.management.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Slf4j
class BookServiceTest {

    /**
     * All data will be loaded before test
     */

    @Autowired
    private MemberService memberService;

    @Autowired
    private BookService bookService;

    @Autowired
    private BorrowService borrowService;


    @Test
    void findBorrowedBooksByMemberAndDateRange() {

        // A new member saved.
        Member savedMember = memberService.saveMember(new MemberDto("Lorem", "Impus", "f"));
        assertNotNull(savedMember);

        // Borrowed a book using saved member's id.
        Book book = bookService.saveBook(new BookDto("Don Quijote", "Cervantes", "Modern Library Classics", "Some Publisher"));

        // Borrowed a book using saved member's id.
        Borrow borrow = borrowService.addBorrow(new BorrowDto(savedMember.getMemberId(), book.getBookId(), new Date()));

        // Retrieved all borrowed books.
        List<Book> bookList = bookService.getBorrowedBooksByMemberAndDateRange(savedMember.getMemberId(), new Date(), new Date()); // today
        log.info("Borrowed book list size: {}", bookList.size());

        assertEquals(book, bookList.get(0));
    }
}
