package com.library.management.model.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
public class BorrowDto {

    @NotNull
    private final long memberId;

    @NotNull
    private final long bookId;

    private final Date borrowedDate;

    private long borrowId;

    private Date returnedDate;

    public BorrowDto(long memberId, long bookId, Date borrowedDate) {
        this.memberId = memberId;
        this.bookId = bookId;
        this.borrowedDate = borrowedDate;
    }
}
