package com.library.management.model.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class BookDto {

    @NotNull
    private final String title;

    @NotNull
    private final String author;

    @NotNull
    private final String genre;

    @NotNull
    private final String publisher;
    private long bookId;

    public BookDto(String title, String author, String genre, String publisher) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.publisher = publisher;
    }
}
