package com.library.management.controller;

import com.library.management.entity.Member;
import com.library.management.model.dto.MemberDto;
import com.library.management.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @GetMapping("/getAll")
    public List<Member> getAll() {
        return memberService.getAll();
    }

    @GetMapping("/borrowed/getAll")
    public List<Member> getBorrowedMembers() {
        return memberService.getBorrowedMembers();
    }

    @PostMapping("/borrowed/getAllByBorrowedDate")
    public List<Member> getBorrowedMembersByBorrowedDate(@RequestParam(value = "borrowedDate") @DateTimeFormat(pattern = "dd/MM/yyyy") Date borrowedDate) {
        return memberService.getBorrowedMembersByBorrowedDate(borrowedDate);
    }

    @GetMapping("/notBorrowed/getNonTerminatedMembers")
    public List<Member> getNonTerminatedNotBorrowedMembers() {
        return memberService.getNonTerminatedNotBorrowedMembers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Member>> getMemberById(@PathVariable(value = "id") long memberId) {
        return ResponseEntity.ok().body(memberService.getMemberById(memberId));
    }

    @PostMapping
    public ResponseEntity<Member> addMember(@Valid @RequestBody MemberDto memberDto) {
        return ResponseEntity.ok().body(memberService.saveMember(memberDto));
    }
}
