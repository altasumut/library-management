package com.library.management.controller;

import com.library.management.entity.Book;
import com.library.management.model.dto.BookDto;
import com.library.management.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("book")
public class BookController {

    @Autowired
    BookService bookService;

    @GetMapping
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Book>> getBookById(@PathVariable(value = "id") long bookId) {
        Optional<Book> book = bookService.getBookById(bookId);
        return ResponseEntity.ok().body(book);
    }

    @GetMapping("/borrowed/getByMemberAndDateRange")
    public List<Book> getBorrowedBooksByMemberAndDateRange(@RequestParam(value = "memberId") long memberId, @RequestParam(value = "startDate") @DateTimeFormat(pattern = "dd/MM/yyyy") Date startDate, @RequestParam(value = "endDate") @DateTimeFormat(pattern = "dd/MM/yyyy") Date endDate) {
        return bookService.getBorrowedBooksByMemberAndDateRange(memberId, startDate, endDate);
    }

    @PostMapping
    public ResponseEntity<Book> addBook(@Valid @RequestBody BookDto bookDto) {
        return ResponseEntity.ok().body(bookService.saveBook(bookDto));
    }
}
