package com.library.management.controller;

import com.library.management.entity.Book;
import com.library.management.entity.Borrow;
import com.library.management.entity.Member;
import com.library.management.model.dto.BorrowDto;
import com.library.management.service.BookService;
import com.library.management.service.BorrowService;
import com.library.management.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("borrow")
@Slf4j
public class BorrowController {

    @Autowired
    BorrowService borrowService;

    @Autowired
    MemberService memberService;

    @Autowired
    BookService bookService;

    @GetMapping
    public List<Borrow> getAllBorrow() {
        return borrowService.getAllBorrows();
    }

    @PostMapping
    public ResponseEntity<Borrow> addBorrow(@Valid @RequestBody BorrowDto borrowDto) {
        Member member = memberService.getMemberById(borrowDto.getMemberId()).orElseThrow(NullPointerException::new);
        Book book = bookService.getBookById(borrowDto.getBookId()).orElseThrow(NullPointerException::new);

        Borrow borrow = borrowService.addBorrow(borrowDto);
        log.info("{} has borrowed of book id {}!", member.getMemberId(), book.getBookId());

        return ResponseEntity.ok().body(borrow);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Borrow> returnBorrow(@PathVariable(value = "id") long borrowId) {
        Borrow borrow = borrowService.returnBorrow(borrowId);

        log.info("{} has returned of book id {}!", borrow.getMemberId(), borrow.getBookId());
        return ResponseEntity.ok().body(borrow);
    }
}
