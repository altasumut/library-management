package com.library.management.service;

import com.library.management.entity.Book;
import com.library.management.model.dto.BookDto;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface BookService {

    List<Book> getAllBooks();

    List<Book> getBorrowedBooksByMemberAndDateRange(long memberId, Date startDate, Date endDate);

    Optional<Book> getBookById(long bookId);

    Book saveBook(BookDto bookDto);
}
