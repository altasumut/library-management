package com.library.management.service.impl;

import com.library.management.entity.Book;
import com.library.management.model.dto.BookDto;
import com.library.management.repository.BookRepository;
import com.library.management.service.BookService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> getBorrowedBooksByMemberAndDateRange(long memberId, Date startDate, Date endDate) {
        return bookRepository.findBorrowedBooksByMemberAndDateRange(memberId, startDate, endDate);
    }

    public Optional<Book> getBookById(long bookId) {
        return bookRepository.findById(bookId);
    }

    public Book saveBook(BookDto bookDto) {
        Book book = modelMapper.map(bookDto, Book.class);
        return bookRepository.save(book);
    }
}
