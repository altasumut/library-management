package com.library.management.service.impl;

import com.library.management.entity.Borrow;
import com.library.management.model.dto.BorrowDto;
import com.library.management.repository.BorrowRepository;
import com.library.management.service.BorrowService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BorrowServiceImpl implements BorrowService {

    @Autowired
    BorrowRepository borrowRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Optional<Borrow> getBorrowById(long borrowId) {
        return borrowRepository.findById(borrowId);
    }

    @Override
    public List<Borrow> getAllBorrows() {
        return borrowRepository.findAll();
    }

    @Override
    public Borrow addBorrow(BorrowDto borrowDto) {
        Borrow borrow = modelMapper.map(borrowDto, Borrow.class);
        return borrowRepository.save(borrow);
    }

    @Override
    public Borrow returnBorrow(long borrowId) {
        Borrow borrow = borrowRepository.findById(borrowId).orElseThrow(NullPointerException::new);
        borrow.setReturnedDate(new Date());
        return borrowRepository.save(borrow);
    }
}
