package com.library.management.service.impl;

import com.library.management.entity.Member;
import com.library.management.model.dto.MemberDto;
import com.library.management.repository.MemberRepository;
import com.library.management.service.MemberService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<Member> getAll() {
        return memberRepository.findAll();
    }

    @Override
    public List<Member> getBorrowedMembers() {
        return memberRepository.findBorrowedMembers();
    }

    @Override
    public List<Member> getBorrowedMembersByBorrowedDate(Date borrowedDate) {
        return memberRepository.findBorrowedMembersByBorrowedDate(borrowedDate);
    }

    @Override
    public List<Member> getNonTerminatedNotBorrowedMembers() {
        return memberRepository.findNonTerminatedNotBorrowedMembers();
    }

    public Optional<Member> getMemberById(long memberId) {
        return memberRepository.findById(memberId);
    }

    public Member saveMember(MemberDto memberDto) {
        Member member = modelMapper.map(memberDto, Member.class);
        return memberRepository.save(member);
    }
}
