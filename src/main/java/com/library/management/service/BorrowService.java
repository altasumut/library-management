package com.library.management.service;

import com.library.management.entity.Borrow;
import com.library.management.model.dto.BorrowDto;

import java.util.List;
import java.util.Optional;

public interface BorrowService {

    Optional<Borrow> getBorrowById(long borrowId);

    List<Borrow> getAllBorrows();

    Borrow addBorrow(BorrowDto borrowDto);

    Borrow returnBorrow(long borrowId);
}
