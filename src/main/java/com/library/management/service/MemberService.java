package com.library.management.service;

import com.library.management.entity.Member;
import com.library.management.model.dto.MemberDto;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface MemberService {

    List<Member> getAll();

    List<Member> getBorrowedMembers();

    List<Member> getBorrowedMembersByBorrowedDate(Date borrowedDate);

    List<Member> getNonTerminatedNotBorrowedMembers();

    Optional<Member> getMemberById(long memberId);

    Member saveMember(MemberDto member);
}
