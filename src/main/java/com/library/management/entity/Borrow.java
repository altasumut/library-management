package com.library.management.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.library.management.util.JsonDataSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Borrow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long borrowId;

    @Column(nullable = false)
    private long memberId;

    @Column(nullable = false)
    private long bookId;

    @Temporal(TemporalType.DATE)
    @JsonSerialize(using = JsonDataSerializer.class)
    @CreatedDate
    private Date borrowedDate;

    @Temporal(TemporalType.DATE)
    @JsonSerialize(using = JsonDataSerializer.class)
    private Date returnedDate;
}
