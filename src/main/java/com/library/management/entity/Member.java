package com.library.management.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.library.management.util.JsonDataSerializer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long memberId;

    private String firstName;

    private String lastName;

    private String gender;

    @Temporal(TemporalType.DATE)
    @CreatedDate
    @JsonSerialize(using = JsonDataSerializer.class)
    private Date joinDate;

    @Column
    @Temporal(TemporalType.DATE)
    @JsonSerialize(using = JsonDataSerializer.class)
    private Date terminationDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Member member = (Member) o;

        if (memberId != member.memberId) return false;
        if (firstName != null ? !firstName.equals(member.firstName) : member.firstName != null) return false;
        if (lastName != null ? !lastName.equals(member.lastName) : member.lastName != null) return false;
        if (gender != null ? !gender.equals(member.gender) : member.gender != null) return false;
        if (joinDate != null ? !joinDate.equals(member.joinDate) : member.joinDate != null) return false;
        return terminationDate != null ? terminationDate.equals(member.terminationDate) : member.terminationDate == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (memberId ^ (memberId >>> 32));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (joinDate != null ? joinDate.hashCode() : 0);
        result = 31 * result + (terminationDate != null ? terminationDate.hashCode() : 0);
        return result;
    }
}
