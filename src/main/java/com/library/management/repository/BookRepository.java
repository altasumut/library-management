package com.library.management.repository;

import com.library.management.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    @Query("select bk from Book bk inner join Borrow br on bk.bookId = br.bookId where br.memberId = :memberId and br.borrowedDate between :startDate and :endDate group by bk")
    List<Book> findBorrowedBooksByMemberAndDateRange(long memberId, Date startDate, Date endDate);
}
