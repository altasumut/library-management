package com.library.management.repository;

import com.library.management.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {

    @Query("select m from Member m inner join Borrow b on m.memberId = b.memberId group by m")
    List<Member> findBorrowedMembers();

    @Query("select m from Member m left join Borrow b on m.memberId = b.memberId where m.terminationDate is null and b.memberId is null group by m")
    List<Member> findNonTerminatedNotBorrowedMembers();

    @Query("select m from Member m inner join Borrow b on m.memberId = b.memberId where b.borrowedDate = :borrowedDate group by m")
    List<Member> findBorrowedMembersByBorrowedDate(Date borrowedDate);
}
