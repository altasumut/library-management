# library-management

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=altasumut_library-management&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=altasumut_library-management)

Project "library-management" is Spring Boot based Java project. It's used H2 in-memory database to not dependent any external database solution. All data will be imported by data.sql which under resources directory.


<img src="https://bitbucket.org/altasumut/library-management/raw/fa8aeadde5664af99dd6ed471d7b4a6741fc38a8/img/ss01.png" width="600">

## Prerequisites
* JDK 1.8+


## Swagger

Please, reach to the API contract in the following link:

```sh
http://localhost:8080/swagger-ui.html
```

<img src="https://bitbucket.org/altasumut/library-management/raw/fa8aeadde5664af99dd6ed471d7b4a6741fc38a8/img/ss03.png" width="600">

## H2 Panel

You can access H2 panel with following link:

```sh
http://localhost:8080/h2-console/

JDBC Url: jdbc:h2:mem:es-chanllenge;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
User name: sa
Password: 
```